#ifndef BST_H
#define BST_H

// * DNT
#include "BSTNode.h/BSTNode.h"
#include <stdio.h>

#ifndef KEY_DEF
#define KEY_DEF
typedef int key;
#endif // ! KEY_DEF

typedef enum { preorder, inorder, postorder } BSTExploreType;

typedef struct BinarySearchTree {
    BSTNode Root;
    // Pointer to the function that frree a single item
    void (*freeItem)(item i);
    /*  Function that return these 3 values
    a >  b : 1
    a == b : 0
    a <  b : -1 */
    int (*compareKeys)(key a, key b);
    // Return the key of an item
    key (*getKey)(item i);
    // Print a item
    void (*printItem)(item i);
} * BST;
// * EDNT

item selectKIteminBST(BST b, uint k);
item searchBST(BST b, key k);
void printBST(BST b, BSTExploreType e);
uint numberOfBSTNodes(BST b);
BST newBST(void (*freeItem)(item i), int (*compareKey)(key a, key b),
           key (*getKey)(item i), void (*printItem)(item i));
void insertInRootBST(BST b, item i);
void insertInLeafBST(BST b, item i);
BSTNode getMinBSTNode(BST b);
item getMinBSTItem(BST b);
BSTNode getMaxBSTNode(BST b);
item getMaxBSTItem(BST b);
void freeBST(BST b, bool deleteItems);

#endif // ! BST_H
