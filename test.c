#include "BST.h"
#include <stdio.h>

int main(int argc, char const *argv[]) {
    // Dataset
    int array[] = {10, 15, 13, 80, 1, 23, 12, 7, 66, 8};
    int *middle;
    BST b;
    uint elementi = 10;

    // Insert in leaf
    puts("Insert data in leaf");
    b = newBST(NULL, NULL, NULL, NULL);
    for (uint i = 0; i < elementi; i++) {
        insertInLeafBST(b, &array[i]);
    }
    elementi = numberOfBSTNodes(b);
    puts("Preorer print");
    printBST(b, preorder);
    puts("Inorder print");
    printBST(b, inorder);
    puts("Postorer print");
    printBST(b, postorder);
    printf("There are %u nodes\n", elementi);
    for (uint i = 1; i <= elementi; i++) {
        middle = selectKIteminBST(b, i);
        printf("The item in %u position in order it's %d\n", i, *middle);
    }
    puts("Free the BST\n");
    freeBST(b, false);

    // Insert in root
    puts("Insert data in root in a new tree");
    b = newBST(NULL, NULL, NULL, NULL);
    for (uint i = 0; i < elementi; i++) {
        insertInRootBST(b, &array[i]);
    }
    elementi = numberOfBSTNodes(b);
    puts("Preorer print");
    printBST(b, preorder);
    puts("Inorder print");
    printBST(b, inorder);
    puts("Postorer print");
    printBST(b, postorder);
    printf("There are %u nodes\n", elementi);
    for (uint i = 1; i <= elementi; i++) {
        middle = selectKIteminBST(b, i);
        printf("The item in %u position in order it's %d\n", i, *middle);
    }
    puts("Free the BST");
    freeBST(b, false);

    // Insert in leaf
    puts("Insert data in leaf");
    b = newBST(NULL, NULL, NULL, NULL);
    for (uint i = 0; i < elementi; i++) {
        insertInLeafBST(b, &array[i]);
    }
    elementi = numberOfBSTNodes(b);
    puts("Preorer print");
    printBST(b, preorder);
    puts("Inorder print");
    printBST(b, inorder);
    puts("Postorer print");
    printBST(b, postorder);
    printf("There are %u nodes\n", elementi);
    for (uint i = 1; i <= elementi; i++) {
        middle = selectKIteminBST(b, i);
        printf("The item in %u position in order it's %d\n", i, *middle);
    }
    puts("Free the BST\n");
    freeBST(b, false);

    // Insert in root
    puts("Insert data in root in a new tree");
    b = newBST(NULL, NULL, NULL, NULL);
    for (uint i = 0; i < elementi; i++) {
        insertInRootBST(b, &array[i]);
    }
    elementi = numberOfBSTNodes(b);
    puts("Preorer print");
    printBST(b, preorder);
    puts("Inorder print");
    printBST(b, inorder);
    puts("Postorer print");
    printBST(b, postorder);
    printf("There are %u nodes\n", elementi);
    for (uint i = 1; i <= elementi; i++) {
        middle = selectKIteminBST(b, i);
        printf("The item in %u position in order it's %d\n", i, *middle);
    }
    puts("Free the BST");
    freeBST(b, false);
    return 0;
}
