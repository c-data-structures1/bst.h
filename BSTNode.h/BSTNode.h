#ifndef BSTNODE_H
#define BSTNODE_H

// * DNT
#include <stdbool.h>
#include <stdlib.h>

#ifndef UINT_DEF
#define UINT_DEF
typedef unsigned int uint;
#endif // ! UINT_DEF
#ifndef ITEM_DEF
#define ITEM_DEF
typedef void *item;
#endif // ! ITEM_DEF
typedef struct BinarySearchTreeNode *BSTNode;
struct BinarySearchTreeNode {
    item Item;
    BSTNode Left;
    BSTNode Right;
};
// * EDNT

void freeBSTNode(BSTNode node, bool deleteItem, void (*freeItem)(item i));
BSTNode newBSTNode(item Item);

#endif // ! BSTNODE_H
