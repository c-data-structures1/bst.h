#include "BSTNode.h"

// Return a new bstNode
BSTNode newBSTNode(item Item) {
    BSTNode n = malloc(sizeof(*n));
    n->Item   = Item;
    n->Left   = NULL;
    n->Right  = NULL;
    return n;
}

// Free a bstNode and if needed his Item
void freeBSTNode(BSTNode node, bool deleteItem, void (*freeItem)(item i)) {
    if (node == NULL) {
        return;
    }
    if (deleteItem) {
        if (freeItem != NULL) {
            if (node->Item != NULL) {
                (*freeItem)(node->Item);
            }
        }
    }
    free(node);
}