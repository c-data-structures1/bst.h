#include "BST.h"

// * STATIC

// ! FAILSAFE FUNCTIONS

// Compare two keys as if they were a numeric value
static int compareKeyFailsafe(key a, key b) {
    // Treat the key as a numeric value
    if (a > b) { return 1; }
    if (a < b) { return -1; }
    return 0;
}

// Call free if item is not NULL
static void freeFailsafe(item i) {
    if (i != NULL) { free(i); }
}

// Treat item as a int pointer and the key as a int, then return the value
// pointed by item
static key getKeyFailSafe(item i) { return (key)(*(int *)i); }

// Treat item as a int pointer and print it
static void printItemFailsafe(item i) { printf("%d - ", *((int *)i)); }

// ! FUNCTIONS FOR VISITS

// Parameters struct for the freeFunctionModel
typedef struct FreeStruct {
    void (*freeItem)(item i);
    bool deleteItem;
} freeStruct;

// Model for freeing BSTNodes
static void freeFunctionModel(BSTNode b, void *args) {
    freeStruct argsDetail = *(freeStruct *)args;
    freeBSTNode(b, argsDetail.deleteItem, argsDetail.freeItem);
}

// Model for printing BSTNodes
static void printFunctionModel(BSTNode b, void *args) {
    void (*printFunction)(item i) = args;
    (*printFunction)(b->Item);
}

// Counter for number of items
static void countItemsModel(BSTNode b, void *args) {
    uint count = *(uint *)args;
    count++;
    *(uint *)args = count;
}

// ! BST UTILITY FUNCTIONS

// Visit the BST preorder and call action on every visited item
static void preorderVisit(BSTNode b, void (*action)(BSTNode n, void *args),
                          void *args) {
    if (b == NULL) { return; }
    (*action)(b, args);
    preorderVisit(b->Left, action, args);
    preorderVisit(b->Right, action, args);
}

// Visit the BST inorder and call action on every visited item
static void inorderVisit(BSTNode b, void (*action)(BSTNode n, void *args),
                         void *args) {
    if (b == NULL) { return; }
    inorderVisit(b->Left, action, args);
    (*action)(b, args);
    inorderVisit(b->Right, action, args);
}

// Visit the BST postorder and call action on every visited item
static void postorderVisit(BSTNode b, void (*action)(BSTNode n, void *args),
                           void *args) {
    if (b == NULL) { return; }
    postorderVisit(b->Left, action, args);
    postorderVisit(b->Right, action, args);
    (*action)(b, args);
}

// Rotate right a BST section
static BSTNode rotateRight(BSTNode root) {
    // Store the values
    BSTNode x = root->Left;
    // Swap the nodes
    root->Left = x->Right;
    x->Right   = root;
    return x; // Return the new root
}

// Rotate left a BST section
static BSTNode rotateLeft(BSTNode root) {
    // Store the values
    BSTNode x = root->Right;
    // Swap the nodes
    root->Right = x->Left;
    x->Left     = root;
    return x; // Return the new root
}

// ! RECURSIVE FUNCTIONS

static void freeNodeRecursive(BSTNode n, bool deleteItems,
                              void (*freeItem)(item i)) {
    if (n == NULL) { return; }
    freeNodeRecursive(n->Left, deleteItems, freeItem);
    freeNodeRecursive(n->Right, deleteItems, freeItem);
    freeBSTNode(n, deleteItems, freeItem);
}

static item getMaxBSTNodeRecursive(BSTNode n) {
    if (n == NULL) { return NULL; }
    item next = getMaxBSTNodeRecursive(n->Right);
    return next == NULL ? n->Item : next;
}

static item getMinBSTNodeRecursive(BSTNode n) {
    if (n == NULL) { return NULL; }
    item next = getMinBSTNodeRecursive(n->Left);
    return next == NULL ? n->Item : next;
}

static uint numberOfBSTNodesFromNode(BSTNode n) {
    uint elements = 0;
    inorderVisit(n, &countItemsModel, (void *)&elements);
    return elements;
}

static BSTNode insertInLeafBSTRecursive(BSTNode root, item n,
                                        int (*compareKeys)(key a, key b),
                                        key (*getKey)(item i)) {
    if (root == NULL) { return newBSTNode(n); }
    switch ((*compareKeys)((*getKey)(root->Item), (*getKey)(n))) {
        case -1:
            root->Right =
                insertInLeafBSTRecursive(root->Right, n, compareKeys, getKey);
            break;

        case 1:
            root->Left =
                insertInLeafBSTRecursive(root->Left, n, compareKeys, getKey);
            break;

        default: break;
    }
    return root;
}

static BSTNode insertInRootBSTRecursive(BSTNode root, item n,
                                        int (*compareKeys)(key a, key b),
                                        key (*getKey)(item i)) {
    if (root == NULL) { return newBSTNode(n); }
    switch ((*compareKeys)((*getKey)(root->Item), (*getKey)(n))) {
        case -1:
            root->Right =
                insertInRootBSTRecursive(root->Right, n, compareKeys, getKey);
            root = rotateLeft(root);
            break;

        case 1:
            root->Left =
                insertInRootBSTRecursive(root->Left, n, compareKeys, getKey);
            root = rotateRight(root);
            break;

        default: break;
    }
    return root;
}

static item searchBSTRecursive(BSTNode n, key k, int (*compare)(key a, key b),
                               key (*getKey)(item i)) {
    if (n == NULL) { return NULL; }

    switch ((*compare)((*getKey)(n->Item), k)) {
        case 1: searchBSTRecursive(n->Left, k, compare, getKey); break;

        case 0: return n->Item; break;

        case -1: searchBSTRecursive(n->Right, k, compare, getKey); break;

        default: return NULL; break;
    }
}

static void reduceIndex(uint *k) {
    uint tmp = *k;
    tmp -= (int)tmp != 0;
    *k = tmp;
}

static item selectKIteminBSTRecursive(BSTNode n, uint *k) {
    if (n == NULL) { return NULL; }

    if (n->Left == NULL && n->Right == NULL) { // If I'm in a end node
        reduceIndex(k);
        return n->Item; // Return the node
    }
    if (n->Left != NULL) {
        item returnValue = selectKIteminBSTRecursive(n->Left, k);
        if (*k == 0) { return returnValue; }
        reduceIndex(k);
        if (*k == 0) { return n->Item; }
    } else {
        reduceIndex(k);
        if (*k == 0) { return n->Item; }
    }
    return selectKIteminBSTRecursive(n->Right, k);
    // if (n->Right != NULL) {
    //     item returnValue = selectKIteminBSTRecursive(n->Right, k);
    //     if (*k == 0) {
    //         return returnValue;
    //     }
    // }
}

// * END OF STATIC

// Free a BST with a preorder visit
void freeBST(BST b, bool deleteItems) {
    freeStruct f;
    f.freeItem   = b->freeItem == NULL ? &freeFailsafe : b->freeItem;
    f.deleteItem = deleteItems;
    // freeNodeRecursive(b->Root, deleteItems, freeFunction);
    postorderVisit(b->Root, &freeFunctionModel, (void *)&f);
    free(b);
}

// Get the max item in the BST
item getMaxBSTItem(BST b) { return getMaxBSTNode(b)->Item; }

// Get the max item in the BST
BSTNode getMaxBSTNode(BST b) { return getMaxBSTNodeRecursive(b->Root); }

// Get the minium item in the BST
item getMinBSTItem(BST b) { return getMinBSTNode(b)->Item; }

// Get the minium item in the BST
BSTNode getMinBSTNode(BST b) { return getMinBSTNodeRecursive(b->Root); }

// Add a item in leaf in a BST
void insertInLeafBST(BST b, item i) {
    // Choose eventual failsafe functions
    int (*compare)(key a, key b) =
        b->compareKeys == NULL ? &compareKeyFailsafe : b->compareKeys;
    key (*get)(item i) = b->getKey == NULL ? &getKeyFailSafe : b->getKey;
    if (b->Root == NULL) { // If the root doesn't exist
        b->Root = newBSTNode(i);
        return;
    }
    insertInLeafBSTRecursive(b->Root, i, compare, get);
}

// Add a item in the root of the BST
void insertInRootBST(BST b, item i) {
    // Choose eventual failsafe functions
    int (*compare)(key a, key b) =
        b->compareKeys == NULL ? &compareKeyFailsafe : b->compareKeys;
    key (*get)(item i) = b->getKey == NULL ? &getKeyFailSafe : b->getKey;

    b->Root = insertInRootBSTRecursive(b->Root, i, compare, get);
}

/*  Return a new BST with Root as bst root
    void freeItem(item i):
        Free the memory for a single item
    bool compareKey(key a, key b):
        Return one fo these 3 values:
        a >  b : 1
        a == b : 0
        a <  b : -1
    key getKey(item i)
        Return the key of an item */
BST newBST(void (*freeItem)(item i), int (*compareKey)(key a, key b),
           key (*getKey)(item i), void (*printItem)(item i)) {
    BST b          = malloc(sizeof(*b));
    b->Root        = NULL;
    b->freeItem    = freeItem;
    b->compareKeys = compareKey;
    b->printItem   = printItem;
    b->getKey      = getKey;
    return b;
}

// Return the number of nodes in the BST
uint numberOfBSTNodes(BST b) { return numberOfBSTNodesFromNode(b->Root); }

// Print an entire BST with the requested visit
void printBST(BST b, BSTExploreType e) {
    void (*printFunction)(item i) =
        b->printItem == NULL ? &printItemFailsafe : b->printItem;
    switch (e) {
        case preorder:
            preorderVisit(b->Root, &printFunctionModel, (void *)printFunction);
            break;

        case inorder:
            inorderVisit(b->Root, &printFunctionModel, (void *)printFunction);
            break;

        case postorder:
            postorderVisit(b->Root, &printFunctionModel,
                           (void *)printFunction);
            break;

        default: break;
    }
    printf("\n");
}

// Search a item in the BST based on it's key, return NULL if it doesn't exist
// of compare function it's missing
item searchBST(BST b, key k) {
    // Choose eventual failsafe functions
    int (*compare)(key a, key b) =
        b->compareKeys == NULL ? &compareKeyFailsafe : b->compareKeys;
    key (*get)(item i) = b->getKey == NULL ? &getKeyFailSafe : b->getKey;

    item result = searchBSTRecursive(b->Root, k, compare, get);
    return result == NULL ? NULL : result;
}

// Select the item in K position when all the BST items are shown in order
item selectKIteminBST(BST b, uint k) {
    uint position = 0;
    return selectKIteminBSTRecursive(b->Root, &k);
}